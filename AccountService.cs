﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    public static class AccountService
    {
        private static User _user;
        public static void Register(Register model)
        {
            var password = PasswordValidator(model.Password, model.ConfirmPassword);

            _user = new User(model.Birthday)
            {
                FullName = $"{model.LastName} {model.FirstName}",
                Gender = model.Gender,
                Email = model.Email,
                Password = password

            };

            Console.WriteLine($"Congratulations!, {_user.FullName} your registration was successful!");

            Console.WriteLine("Press 1 to login:\n");
            var input = Console.ReadLine();

            while (input != "1")
            {
                Console.Write("The instruction says input 1. Try again ");
                input = Console.ReadLine();
            }

            Login_Details:
                Console.WriteLine("Enter your Email and password seperated with space");
                var credentials = Console.ReadLine();

            while (string.IsNullOrWhiteSpace(credentials) || 
                credentials.Split().Length <= 1 ||
                credentials.Split().Length > 3)
            {
                goto Login_Details;
            }

            var email = credentials.Split()[0].Trim().ToLower();
            var passWord = credentials.Split()[1].Trim().ToLower();

            Login(email, passWord);

        }

        public static void Login(string email, string password)
        {
            while (_user.Email.ToLower() != email.ToLower() && _user.Password != password)
            {
                Console.WriteLine("Invalid Login Details");
                Console.WriteLine("Enter your Email and password seperated with space");
                var credentials = Console.ReadLine();
                email = credentials.Split()[0].Trim().ToLower();
                password = credentials.Split()[1].ToLower();

            }

            _user.ToggleAgePrivacy();
            _user.ToggleAgePrivacy();

            Console.WriteLine($"Welcome, {_user.FullName} Your Age is {_user.Age} Joined: {_user.Created}");

        }

        private static string PasswordValidator( string password, string confirmPassword)
        {


            while(password.Trim() != confirmPassword.Trim())
            {
                Console.WriteLine("Passwords don't match");

                Console.Write("Input password ");
                password = Console.ReadLine();
                Console.Write("Confirm password ");
                confirmPassword = Console.ReadLine();

            }

            return password;

        }
    }
}
