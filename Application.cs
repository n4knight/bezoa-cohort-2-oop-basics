﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OOP
{
    public static class Application
    {
        public static void Run()
        {
            var menu = new StringBuilder();
            menu.Append("Hello, welcome to my App:\n");
            menu.AppendLine("Enter your FirstName");
            Console.WriteLine(menu.ToString());
            var firstName = Console.ReadLine();

            Console.WriteLine("Enter Lastname");
            var lastName = Console.ReadLine();

            while (isNumber(firstName))
            {
                Console.WriteLine("You need to provide a Firstname ");

                Console.Write("Enter Firstname ");
                firstName = Console.ReadLine();

            }

            while (isNumber(lastName))
            {
                Console.WriteLine("You need a lastname ");

                Console.Write("Enter Lastname ");
                lastName = Console.ReadLine();

            }

            Console.WriteLine("Enter email");
            var email = Console.ReadLine();

            while (!isEmail(email))
            {
                Console.Write("Invalid email format. input again ");
                email = Console.ReadLine();
            }

            Console.WriteLine("Enter birthday - MM-DD-YYYY");
            var birthday = Console.ReadLine();
            while (!isVlaidDate(birthday))
            {
                Console.Write("Wrong date format (mm/dd/yy) ");
                birthday = Console.ReadLine();
            }

            Console.WriteLine("Select your Gender: \n 1. male \n 2. Female \n 3. Prefer not to say");
            var gender = Console.ReadLine();
            while(gender != "1" && gender != "2" && gender != "3")
            {
                Console.WriteLine("Invalid Gender Selection");
                Console.Write("Select your Gender: \n 1. male \n 2. Female \n 3. Prefer not to say ");
                gender = Console.ReadLine();
            }

            var selectedGender = GenderSelection(gender);
            Console.WriteLine("Enter Password");
            var password = Console.ReadLine();

            Console.WriteLine("Confirm Password");
            var confirmPassword = Console.ReadLine();

            while (string.IsNullOrWhiteSpace(password))
            {
                Console.WriteLine("Password Does not match!! Input again ");

                Console.Write("Enter Password ");
                password = Console.ReadLine();

            }

            while (string.IsNullOrWhiteSpace(confirmPassword))
            {
                Console.WriteLine("Password Does not match!! Input again ");

                Console.Write("Confirm Password ");
                confirmPassword = Console.ReadLine();

            }

            var formData = new Register { 
                FirstName = firstName, 
                LastName = lastName,
                Email = email,
                Birthday = DateTime.Parse(birthday),
                Password = password,
                ConfirmPassword = confirmPassword,
                Gender = selectedGender };

            AccountService.Register(formData);
        }

        public static Gender GenderSelection(string gender)
        {
            
            switch (gender)
            {
                case "1":
                    return Gender.Male;       
                case "2":
                    return Gender.Female;
                case "3":
                    return Gender.PreferNotToSay;
                default:
                    return Gender.SelectGender;
            }
        }

         static bool isNumber (string input)
        {

            return int.TryParse(input, out _) || string.IsNullOrWhiteSpace(input) || input.Length < 3;

        }

         static bool isEmail (string input)
        {
            string emailPattern = @"^((\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)\s*[;,.]{0,1}\s*)+$";
            Regex regex = new Regex(emailPattern);
            Match match = regex.Match(input);

            if (match.Success) return true;

            return false;
        }

        static bool isVlaidDate(string input)
        {
            return DateTime.TryParse(input, out DateTime correctDate);
        }

    }
}
